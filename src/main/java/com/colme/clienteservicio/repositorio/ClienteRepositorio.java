package com.colme.clienteservicio.repositorio;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.colme.clienteservicio.modelo.Cliente;

/**
 * Interface para definir las operaciones de base de datos relacionadas con
 * el cliente
 * 
 * @author Oscar Villarreal
 *
 */
public interface ClienteRepositorio extends JpaRepository<Cliente, Long> {
	
	public Cliente findById(long id);
	
	public List<Cliente> findByNumeroDocumento(String numeroDocumento);

}
