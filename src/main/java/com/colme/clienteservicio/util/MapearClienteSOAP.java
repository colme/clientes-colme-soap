package com.colme.clienteservicio.util;

import java.util.List;

import com.colme.clienteservicio.modelo.Cliente;
import com.colme.clienteservicio.soap.ws.ClienteSOAP;
import com.colme.clienteservicio.soap.ws.GetCrearClienteRequest;
import com.colme.clienteservicio.soap.ws.ListaClientes;

public class MapearClienteSOAP{
	
	public static GetCrearClienteRequest mapearCliente(Cliente cliente) {
		GetCrearClienteRequest clienteSOAP = new GetCrearClienteRequest();
		clienteSOAP.setNombres(cliente.getNombres());
		clienteSOAP.setApellidos(cliente.getApellidos());
		clienteSOAP.setTipoDocumento(cliente.getTipoDocumento());
		clienteSOAP.setNumeroDocumento(cliente.getNumeroDocumento());
		clienteSOAP.setFechaNacimiento(UtilWebService.modificarFechaToString(cliente.getFechaNacimiento()));
		clienteSOAP.setFechaVinculacion(UtilWebService.modificarFechaToString(cliente.getFechaVinculacion()));
		clienteSOAP.setCargo(cliente.getCargo());
		clienteSOAP.setSalario(cliente.getSalario());
		return clienteSOAP;
	}
	
	public static ClienteSOAP mapearClienteSOAP(Cliente Cliente) {
		ClienteSOAP clienteSOAP = new ClienteSOAP();
		clienteSOAP.setIdCliente(Cliente.getIdCliente());
		clienteSOAP.setNombres(Cliente.getNombres());
		clienteSOAP.setApellidos(Cliente.getApellidos());
		clienteSOAP.setTipoDocumento(Cliente.getTipoDocumento());
		clienteSOAP.setNumeroDocumento(Cliente.getNumeroDocumento());
		clienteSOAP.setFechaNacimiento(UtilWebService.modificarFechaToString(Cliente.getFechaNacimiento()));
		clienteSOAP.setFechaVinculacion(UtilWebService.modificarFechaToString(Cliente.getFechaVinculacion()));
		clienteSOAP.setCargo(Cliente.getCargo());
		clienteSOAP.setSalario(Cliente.getSalario());
		return clienteSOAP;
	}
	
	public static ListaClientes mapearListaClienteSOAP(List<Cliente> clientes) {
		ListaClientes listaClienteSOAP = new ListaClientes();
		for (Cliente cliente : clientes) {
			listaClienteSOAP.getClienteSOAP().add(mapearClienteSOAP(cliente));
		}
		return listaClienteSOAP;
	}
}
