package com.colme.clienteservicio.util;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class UtilWebService {

	/**
	 * Devuelve un objeto XMLGregorianCalendar a partir de un objeto Date
	 * 
	 * @param fecha
	 * @return
	 */
	public static String modificarFechaToString(LocalDate fecha) {
		if(fecha!=null) {
			return fecha.format(DateTimeFormatter.ofPattern("yyyy/MM/dd"));
		}
		return null;
	}
	
	/**
	 * Devuelve un objeto XMLGregorianCalendar a partir de un objeto Date.
	 * 
	 * @param fecha
	 * @return
	 */
	public static LocalDate modificarFechaToDate(String fecha) {
        return LocalDate.parse(fecha, DateTimeFormatter.ofPattern("yyyy/MM/dd"));
	}
}
