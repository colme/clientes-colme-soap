package com.colme.clienteservicio.soap.endpoint;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.colme.clienteservicio.modelo.Cliente;
import com.colme.clienteservicio.servicio.ClienteServicio;
import com.colme.clienteservicio.soap.ws.ClienteSOAP;
import com.colme.clienteservicio.soap.ws.GetClienteIdRequest;
import com.colme.clienteservicio.soap.ws.GetCrearClienteRequest;
import com.colme.clienteservicio.soap.ws.ListaClientes;
import com.colme.clienteservicio.util.MapearClienteSOAP;
import com.colme.clienteservicio.util.UtilWebService;

@Endpoint
public class CargaClienteEndpoint {

	private static final String NAMESPACE = "http://www.colme.com/clienteservicio/soap/ws";

	@Autowired
	private ClienteServicio service;

	@PayloadRoot(namespace = NAMESPACE, localPart = "GetCrearClienteRequest")
	@ResponsePayload
	public ClienteSOAP crearCliente(@RequestPayload GetCrearClienteRequest request) {
		ClienteSOAP Cliente = MapearClienteSOAP.mapearClienteSOAP(
				service.crear(new Cliente(request.getNombres(), request.getApellidos(), request.getTipoDocumento(),
						request.getNumeroDocumento(), UtilWebService.modificarFechaToDate(request.getFechaNacimiento()),
						UtilWebService.modificarFechaToDate(request.getFechaVinculacion()), request.getCargo(),
						request.getSalario())));
		return Cliente;
	}

	@PayloadRoot(namespace = NAMESPACE, localPart = "GetClienteIdRequest")
	@ResponsePayload
	public ClienteSOAP ClienteId(@RequestPayload GetClienteIdRequest request) {
		Cliente Cliente = service.clienteID(request.getIdCliente());
		if(Cliente!=null) {
			return MapearClienteSOAP.mapearClienteSOAP(Cliente);
		}
		return null;
	}
	
	@PayloadRoot(namespace = NAMESPACE, localPart = "GetListarClienteIdRequest")
	@ResponsePayload
	public ListaClientes ListarClientes() {
		List<Cliente> Clientes = service.listarClientes();
		if(!Clientes.isEmpty()) {
			return MapearClienteSOAP.mapearListaClienteSOAP(Clientes);
		}
		return null;
	}
}
