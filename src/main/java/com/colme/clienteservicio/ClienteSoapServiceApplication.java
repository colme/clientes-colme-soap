package com.colme.clienteservicio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClienteSoapServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClienteSoapServiceApplication.class, args);
	}

}
