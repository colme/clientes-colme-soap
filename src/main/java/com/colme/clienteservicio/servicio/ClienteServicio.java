package com.colme.clienteservicio.servicio;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.colme.clienteservicio.modelo.Cliente;
import com.colme.clienteservicio.repositorio.ClienteRepositorio;

/**
 * Clase para definir los servicios del cliente
 * 
 * @author Oscar Villarreal
 *
 */
@Service
public class ClienteServicio {

	private final ClienteRepositorio clienteRepositorio;

	public ClienteServicio(ClienteRepositorio clienteRepositorio) {
		this.clienteRepositorio = clienteRepositorio;
	}

	@Transactional
	public Cliente crear(Cliente cliente) {
		return this.clienteRepositorio.save(cliente);
	}
	
	public Cliente clienteID(long id) {
		return this.clienteRepositorio.findById(id);
	}
	
	public List<Cliente> listarClientes() {
		return this.clienteRepositorio.findAll();
	}
	
	@Transactional
	public void eliminarcliente(long id) {
		this.clienteRepositorio.deleteById(id);
	}
	
	@Transactional
	public Cliente modificarcliente(Cliente cliente) {
		return this.clienteRepositorio.save(cliente);
	}
	
	public List<Cliente> findByNumeroDocumento(String numeroDocumento) {
		return this.clienteRepositorio.findByNumeroDocumento(numeroDocumento);
	}
}
